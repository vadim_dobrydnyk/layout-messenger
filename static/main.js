document.addEventListener('DOMContentLoaded', function(event) {
  document
    .querySelector('.top-bar__back')
    .addEventListener('click', showCorrespondenceList);
  document.querySelectorAll('.list .item').forEach(function(item, i, arr) {
    item.addEventListener('click', showCorrespondence);
  });
});

function showCorrespondenceList() {
  if (media()) {
    document.querySelector('.messenger').style.transform = 'translateX(100%)';
    document.querySelector('.users').style.transform = 'translateX(0%)';
    document.querySelector('.users').style.flex = '0 0 100%';
  }
}

function showCorrespondence() {
  if (media()) {
    document.querySelector('.messenger').style.transform = 'translateX(-100%)';
    document.querySelector('.messenger').style.flex = '0 0 100%';
    document.querySelector('.users').style.transform = 'translateX(-100%)';
  }
}

function setNormal() {
  document.querySelector('.users').style.transform = 'translateX(0%)';
  document.querySelector('.messenger').style.transform = 'translateX(0%)';
}

function media() {
  return window.matchMedia('(max-width: 768px)').matches;
}
